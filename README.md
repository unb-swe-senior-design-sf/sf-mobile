# sf-mobile

# Getting Started

1. Install Android Studio  
https://developer.android.com/studio

Android Studio has built-in support for Android development that you won't get out of the box with other IDEs.

2. Set an Android Virtual Device (AVD)
- Tools -> AVD Manager
- Create a new virtual device. Any Android phone should suffice. Make sure to pick an API level of 27.
- You should now be able to select the device you've created and run your app by the pressing the green play button in the IDE.


# Android App Development
- Most of your Java code will go into the `/SmartFridge/app/src/main/java/com/unb/smartfridge` folder
- All of the resources are found at `/SmartFridge/app/src/main/res`
- Each view of an Android app will consist of Java code and an XML file which defines the layout of the view
- Although the IDE provides a GUI for editing the layout of the views, editing the XML is considered best practice.

# Design Guidelines
- The app will follow the specifications laid out in the Material Design Guide by Google  
https://material.io/design/

- Ensure that values are not hardcoded into layouts by using the `values` directory. 
For example, this directory will contain XML files that specify colour and string values.
This practice ensures that the entire app can be quickly updated by editing only a single file.

**Example**
```
# This means go look at the colors.xml file in the values directory for the value of 'colorPrimary'
app:itemBackground="@color/colorPrimary"
```

