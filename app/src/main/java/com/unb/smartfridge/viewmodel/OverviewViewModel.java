package com.unb.smartfridge.viewmodel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.unb.smartfridge.service.model.Fridge;
import com.unb.smartfridge.service.model.TemperatureReading;
import com.unb.smartfridge.service.repository.FridgeRepository;
import com.unb.smartfridge.service.repository.ItemRepository;
import com.unb.smartfridge.viewmodel.util.TemperatureReadingFormatter;

public class OverviewViewModel extends AndroidViewModel {

    private final String TAG = "OverviewViewModel";

    private FridgeRepository fridgeRepo;
    private ItemRepository itemRepo;

    private String latestTempReading;
    private String latestUpdateTime;

    private String fridgeName;

    private String totalItems;
    private String totalExpiredItems;
    private String totalNonoptimalTemperatureItems;

    public OverviewViewModel(@NonNull Application application) {
        super(application);
        fridgeRepo = new FridgeRepository(application);
        itemRepo = new ItemRepository(application);

        try {
            TemperatureReading tempReading = getLatestTemperatureReading();
            latestTempReading = TemperatureReadingFormatter.formatTemperatureValue(tempReading.getTemperatureValue());
            latestUpdateTime = TemperatureReadingFormatter.formatLastUpdateTime(tempReading.getTimestamp());

            Fridge fridge = getFridgeById(1);
            fridgeName = fridge.getName();

            totalItems = Integer.toString(getTotalItemCount());
        } catch (NullPointerException ex) {
            // Database values may not exist if this is the first time the app is launched
            Log.d(TAG, "Database values can not be retrieved.");
        }
    }

    private TemperatureReading getLatestTemperatureReading() {
        return fridgeRepo.getLatestTemperatureReading();
    }

    private Fridge getFridgeById(int id) {
        return fridgeRepo.getFridgeById(id);
    }

    private int getTotalItemCount() {
        return itemRepo.getItemCount();
    }

    public String getLatestTempReading() {
        return latestTempReading;
    }

    public String getLatestUpdateTime() {
        return latestUpdateTime;
    }

    public String getFridgeName() {
        return fridgeName;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public String getTotalExpiredItems() {
        return totalExpiredItems;
    }

    public String getTotalNonoptimalTemperatureItems() {
        return totalNonoptimalTemperatureItems;
    }
}
