package com.unb.smartfridge.viewmodel.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TemperatureReadingFormatter {

    public static String formatTemperatureValue(double temperature) {
        DecimalFormat decimalFormatter= new DecimalFormat("###.#");
        String formattedTemperature = decimalFormatter.format(temperature);
        formattedTemperature += "°C";

        return formattedTemperature;
    }

    public static String formatLastUpdateTime(Date timestamp) {
        DateFormat dateFormat = new SimpleDateFormat("h:mm a");
        String hourMonthTime = dateFormat.format(timestamp);
        return "Last Update: " + hourMonthTime;
    }
}
