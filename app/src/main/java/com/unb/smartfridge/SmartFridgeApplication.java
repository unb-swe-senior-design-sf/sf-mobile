package com.unb.smartfridge;

import android.app.Application;

public class SmartFridgeApplication extends Application {

    private static SmartFridgeApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static SmartFridgeApplication getInstance() {
        return instance;
    }
}
