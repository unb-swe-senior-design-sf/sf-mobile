package com.unb.smartfridge.service.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.unb.smartfridge.service.model.ActivityItem;
import com.unb.smartfridge.service.model.ActivityItemDao;
import com.unb.smartfridge.service.model.AppDatabase;
import com.unb.smartfridge.service.model.Fridge;
import com.unb.smartfridge.service.model.FridgeDao;
import com.unb.smartfridge.service.model.ItemDao;
import com.unb.smartfridge.service.model.ItemPreference;
import com.unb.smartfridge.service.model.ItemPreferenceDao;
import com.unb.smartfridge.service.model.ScanEvent;
import com.unb.smartfridge.service.model.ScanEventDao;
import com.unb.smartfridge.service.model.TemperatureReading;
import com.unb.smartfridge.service.model.TemperatureReadingDao;

import java.util.Date;
import java.util.List;

public class FridgeRepository {

    private AppDatabase databaseService;
    private ScanEventDao scanEventDao;
    private FridgeDao fridgeDao;
    private ItemPreferenceDao itemPreferenceDao;
    private TemperatureReadingDao temperatureReadingDao;
    private ActivityItemDao activityItemDao;
    private ItemDao itemDao;


    public FridgeRepository(Application application) {
        this.databaseService = AppDatabase.getInstance(application);
        itemDao = databaseService.itemDao();
        scanEventDao = databaseService.scanEventDao();
        fridgeDao = databaseService.fridgeDao();
        itemPreferenceDao = databaseService.itemPreferenceDao();
        temperatureReadingDao = databaseService.temperatureReadingDao();
        activityItemDao = databaseService.activityItemDao();
    }

    public List<ScanEvent> getAllScanEvents() {
        return scanEventDao.getAllScanEvents();
    }

    public List<ScanEvent> getScanEventsBetweenDates(Date to, Date from) {
        return scanEventDao.getScanEventsBetweenDates(to, from);
    }

    public ScanEvent getScanEventsById(int id) {
        return scanEventDao.getScanEventsById(id);
    }

    public long insertScanEvent(int itemId, Date timestamp, boolean checkIn) {
        ScanEvent newEvent = new ScanEvent(itemId, timestamp, checkIn);

        String scanTypeTitle = checkIn ? "Scan In" : "Scan Out";
        String scanType = checkIn ? "added" : "removed";
        String itemName = itemDao.getItemNameByUpc(itemDao.getItemById(itemId).getItemUpc());
        ActivityItem newActivity = new ActivityItem(1, scanTypeTitle, itemName + " was " +  scanType, timestamp);
        activityItemDao.insertActivity(newActivity);

        return scanEventDao.insertScanEvent(newEvent);
    }

    public long insertScanEvent(ScanEvent scanEvent) {
        int itemId = scanEvent.getItemId();

        return scanEventDao.insertScanEvent(scanEvent);
    }

    public List<Fridge> getAllFridges() {
        return fridgeDao.getAllFridges();
    }

    public Fridge getFridgeById(int fridgeId) {
        return fridgeDao.getFridgeById(fridgeId);
    }

    public long insertFridge(String name) {
        Fridge newFridge = new Fridge(name);
        return fridgeDao.insertFridge(newFridge);
    }

    public void deleteFridge(Fridge fridge) {
        fridgeDao.deleteFridge(fridge);
    }

    public void updateFridge(Fridge fridge) {
        fridgeDao.updateFridge(fridge);
    }

    public List<ItemPreference> getAllItemPreferences() {
        return itemPreferenceDao.getAllItemPreferences();
    }

    public ItemPreference getItemPreferenceById(int preferenceId) {
        return itemPreferenceDao.getItemPreferenceById(preferenceId);
    }

    public long insertItemPreference(int preferenceId, float itemUpc, String itemAlias, byte[] itemImage) {
        ItemPreference newItemPreference = new ItemPreference(preferenceId, itemUpc, itemAlias, itemImage);
        return itemPreferenceDao.insertItemPreference(newItemPreference);
    }

    public void deleteItemPreference(ItemPreference itemPreference) {
        itemPreferenceDao.deleteItemPreference(itemPreference);
    }

    public void updateItemPreference(ItemPreference itemPreference) {
        itemPreferenceDao.updateItemPreference(itemPreference);
    }

    public List<TemperatureReading> getAllTemperatureReadings() {
        return temperatureReadingDao.getAllTemperatureReadings();
    }

    public TemperatureReading getTemperatureReadingById(int readingId) {
        return temperatureReadingDao.getTemperatureReadingById(readingId);
    }

    public List<TemperatureReading> getTemperatureReadingsBetweenDates(Date from, Date to) {
        return temperatureReadingDao.getTemperatureReadingsBetweenDates(from, to);
    }

    public long insertTemperatureReading(int fridgeId, Date timestamp, double temperatureValue) {
        TemperatureReading newTemperatureReading = new TemperatureReading(fridgeId, timestamp, temperatureValue);
        return temperatureReadingDao.insertTemperatureReading(newTemperatureReading);
    }

    public TemperatureReading getLatestTemperatureReading() {
        return temperatureReadingDao.getLatestingTemperatureReading();
    }

    public LiveData<List<ActivityItem>> getAllActivities() {
        List<ActivityItem> list = activityItemDao.getAllActivities().getValue();
        return activityItemDao.getAllActivities();
    }

    public ActivityItem getActivityById(int activityId) {
        return activityItemDao.getActivityById(activityId);
    }

    public long insertActivity(ActivityItem activityItem) {
        return activityItemDao.insertActivity(activityItem);
    }

    public void deleteActivity(ActivityItem activityItem) {
        activityItemDao.deleteActivity(activityItem);
    }

    public void updateActivity(ActivityItem activityItem) {
        activityItemDao.updateActivity(activityItem);
    }

    public LiveData<List<ActivityItem>> getRecentActivityItems(int limit) {
        return activityItemDao.getRecentActivityItems(limit);
    }
}
