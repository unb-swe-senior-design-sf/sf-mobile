package com.unb.smartfridge.service.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.Date;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey(entity = Fridge.class,
        parentColumns = "fridgeId",
        childColumns = "fridgeId",
        onDelete = CASCADE))
public class TemperatureReading {

    @PrimaryKey(autoGenerate = true)
    private int readingId;

    @ColumnInfo(index = true)
    private int fridgeId;

    @TypeConverters({Converter.class})
    private Date timestamp;

    private double temperatureValue;

    public TemperatureReading(int fridgeId, Date timestamp, double temperatureValue) {
        this.fridgeId = fridgeId;
        this.timestamp = timestamp;
        this.temperatureValue = temperatureValue;
    }

    public void setReadingId(int readingId) {
        this.readingId = readingId;
    }

    public int getReadingId() {
        return readingId;
    }

    public int getFridgeId() {
        return fridgeId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public double getTemperatureValue() {
        return temperatureValue;
    }
}
