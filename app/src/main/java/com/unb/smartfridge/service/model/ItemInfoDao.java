package com.unb.smartfridge.service.model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ItemInfoDao {

    @Query("SELECT * FROM ItemInfo")
    List<ItemInfo> getAllItemInfo();

    @Query("SELECT * FROM ItemInfo WHERE itemUpc = :itemUpc")
    ItemInfo getItemInfoById(String itemUpc);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertItemInfo(ItemInfo itemInfo);

    @Delete
    void deleteItemInfo(ItemInfo itemInfo);

    @Update
    void updateItemInfo(ItemInfo itemInfo);
}
