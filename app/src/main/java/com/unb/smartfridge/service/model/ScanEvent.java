package com.unb.smartfridge.service.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.Date;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey(entity = Item.class,
        parentColumns = "itemId",
        childColumns = "itemId",
        onDelete = CASCADE))
public class ScanEvent {

    @PrimaryKey(autoGenerate = true)
    private int eventId;

    @TypeConverters({Converter.class})
    private Date timestamp;

    @ColumnInfo(index = true)
    private int itemId;

    private boolean checkIn;

    public ScanEvent(int itemId, Date timestamp, boolean checkIn) {
        this.timestamp = timestamp;
        this.itemId = itemId;
        this.checkIn = checkIn;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public int getEventId() {
        return eventId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public int getItemId() {
        return itemId;
    }

    public boolean isCheckIn() {
        return checkIn;
    }
}
