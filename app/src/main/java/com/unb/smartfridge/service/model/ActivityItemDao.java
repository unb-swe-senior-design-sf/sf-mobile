package com.unb.smartfridge.service.model;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ActivityItemDao {

    @Query("SELECT * FROM ActivityItem")
    LiveData<List<ActivityItem>> getAllActivities();

    @Query("SELECT * FROM ActivityItem WHERE activityId = :activityId")
    ActivityItem getActivityById(int activityId);

    @Query("SELECT * FROM ActivityItem ORDER BY activityId DESC LIMIT :limit")
    LiveData<List<ActivityItem>> getRecentActivityItems(int limit);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertActivity(ActivityItem activityItem);

    @Delete
    void deleteActivity(ActivityItem activityItem);

    @Update
    void updateActivity(ActivityItem activityItem);
}
