package com.unb.smartfridge.service.repository;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.unb.smartfridge.service.model.AppDatabase;
import com.unb.smartfridge.service.model.Item;
import com.unb.smartfridge.service.model.ItemDao;
import com.unb.smartfridge.service.model.ItemInfoDao;
import com.unb.smartfridge.service.model.ScanEvent;
import com.unb.smartfridge.service.model.ScanEventDao;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ItemRepository {

    private final String TAG = "ItemRepository";

    private AppDatabase databaseService;
    private ItemDao itemDao;
    private ItemInfoDao itemInfoDao;
    private ScanEventDao scanEventDao;

    public ItemRepository(Application application) {
        this.databaseService = AppDatabase.getInstance(application);
        itemDao = databaseService.itemDao();
        itemInfoDao = databaseService.itemInfoDao();
        scanEventDao = databaseService.scanEventDao();
    }

    public List<Item> getAllItems() {
        return itemDao.getAllItems();
    }

    public Item getItemById(int itemId) {
        return itemDao.getItemById(itemId);
    }

    public long insertItem(String itemUpc, int fridgeId, Date timestamp) {
        int expirationInterval = itemInfoDao.getItemInfoById(itemUpc).getExpirationInterval();
        Date expirationDate  = determineExpirationDate(expirationInterval, timestamp);

        Item newItem = new Item(itemUpc, fridgeId, expirationDate);
        return itemDao.insertItem(newItem);
    }

    public long insertItem(Item item) {
        return itemDao.insertItem(item);
    }

    public void insertItemAndScanEvent(String itemUpc, int fridgeId, Date timestamp, boolean checkIn) {
        int expirationInterval = itemInfoDao.getItemInfoById(itemUpc).getExpirationInterval();
        Date expirationDate  = determineExpirationDate(expirationInterval, timestamp);

        Item newItem = new Item(itemUpc, fridgeId, expirationDate);
        ScanEvent scanEvent = new ScanEvent(newItem.getItemId(), timestamp, checkIn);
        insertItem(newItem);
        scanEventDao.insertScanEvent(scanEvent);
    }

    public int getItemCount() {
        return itemDao.getItemCount();
    }

    public List<Integer> updateExpiredItems() {
        List<Item> allItems = null;
        List<Integer> itemIds = null;
        try {
            allItems = getAllItems();
            itemIds = getNewExpiredItemIds(allItems);
            itemDao.updateExpiredItems(itemIds);
        } catch (NullPointerException nullEx) {
            Log.d(TAG, nullEx.getMessage());
            Log.d(TAG, "Unable to update expired item status as there are no items in the database.");
        }

        return itemIds;
    }

    public List<Item> getExpiredItems() {
        return itemDao.getExpiredItems();
    }

    public void deleteItem(Item item) {
        itemDao.deleteItem(item);
    }

    public void updateItem(Item item) {
        itemDao.updateItem(item);
    }

    public String getItemNameByUpc(String upc) { return itemDao.getItemNameByUpc(upc); }

    private Date determineExpirationDate(int expirationInterval, Date checkinDate) {

        if (expirationInterval == 0) {
            return null;
        }

        LocalDate checkinLocalDate = checkinDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate expirationLocalDate = checkinLocalDate.plus(expirationInterval, ChronoUnit.DAYS);
        Date expirationDate = java.util.Date.from(expirationLocalDate.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());

        return expirationDate;
    }

    private List<Integer> getNewExpiredItemIds(List<Item> allItems) {
        List<Integer> expiredItemIds = new ArrayList<>();
        Date currentDate = new Date();

        for (Item item: allItems) {
            boolean isAlreadyExpired = item.getIsExpired();
            if (!isAlreadyExpired) {
                Log.d(TAG, "Determined that item with UPC [" + item.getItemUpc() + "] has expired");
                boolean itemIsExpired = determineItemIsExpired(item);

                if (itemIsExpired) {
                    expiredItemIds.add(item.getItemId());
                }
            }
        }

        return expiredItemIds;
    }

    private boolean determineItemIsExpired(Item item) {
        Date currentDate = new Date();
        Date itemExpirationDate = item.getExpirationDate();

        return currentDate.compareTo(itemExpirationDate) >= 0;
    }
}
