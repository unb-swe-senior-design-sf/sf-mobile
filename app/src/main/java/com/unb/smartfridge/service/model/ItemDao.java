package com.unb.smartfridge.service.model;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ItemDao {

    @Query("SELECT * FROM Item")
    List<Item> getAllItems();

    @Query("SELECT * FROM Item WHERE itemId = :itemId")
    Item getItemById(int itemId);

    @Query("SELECT name FROM ItemInfo WHERE name LIKE :upc")
    String getItemNameByUpc(String upc);

    @Query("SELECT COUNT(*) FROM Item")
    int getItemCount();

    @Query("UPDATE Item SET isExpired = 1 WHERE itemId IN (:itemIds)")
    void updateExpiredItems(List<Integer> itemIds);


    @Query("SELECT * FROM Item WHERE isExpired = 1")
    List<Item> getExpiredItems();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertItem(Item item);

    @Delete
    void deleteItem(Item item);

    @Update
    void updateItem(Item item);
}
