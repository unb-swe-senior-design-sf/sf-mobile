package com.unb.smartfridge.service.model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface FridgeDao {

    @Query("SELECT * FROM Fridge")
    List<Fridge> getAllFridges();

    @Query("SELECT * FROM FRIDGE WHERE fridgeId = :fridgeId")
    Fridge getFridgeById(int fridgeId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertFridge(Fridge fridge);

    @Delete
    void deleteFridge(Fridge fridge);

    @Update
    void updateFridge(Fridge fridge);
}
