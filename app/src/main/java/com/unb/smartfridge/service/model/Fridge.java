package com.unb.smartfridge.service.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Fridge {

    @PrimaryKey(autoGenerate = true)
    private int fridgeId;

    private String name;

    public Fridge(String name) {
        this.name = name;
    }

    public void setFridgeId(int fridgeId) {
        this.fridgeId = fridgeId;
    }

    public int getFridgeId() {
        return fridgeId;
    }

    public String getName() {
        return name;
    }
}
