package com.unb.smartfridge.service.model;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.TypeConverters;

import java.util.Date;
import java.util.List;

@Dao
@TypeConverters(Converter.class)
public interface ScanEventDao {

    @Query("SELECT * FROM ScanEvent")
    List<ScanEvent> getAllScanEvents();

    @Query("SELECT * FROM ScanEvent WHERE itemId = :itemId")
    ScanEvent getScanEventsById(int itemId);

    @Query("SELECT * FROM ScanEvent WHERE timestamp BETWEEN :from AND :to")
    List<ScanEvent> getScanEventsBetweenDates(Date from, Date to);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertScanEvent(ScanEvent scanEvent);
}
