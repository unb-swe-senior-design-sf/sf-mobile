package com.unb.smartfridge.service.model;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Database(entities =
            {Fridge.class, Item.class, ItemInfo.class,
            ItemPreference.class, ScanEvent.class, TemperatureReading.class, ActivityItem.class},
        version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private final String TAG = "AppDatabase";

    private static AppDatabase instance;
    public abstract FridgeDao fridgeDao();
    public abstract ItemInfoDao itemInfoDao();
    public abstract ItemDao itemDao();
    public abstract ItemPreferenceDao itemPreferenceDao();
    public abstract ScanEventDao scanEventDao();
    public abstract TemperatureReadingDao temperatureReadingDao();
    public abstract ActivityItemDao activityItemDao();

    public static synchronized  AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    AppDatabase.class,
                    "AppDatabase")
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries() // Forgive me, for I have sinned
                    .addCallback(roomCallback)
                    .build();
        }

        return instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private FridgeDao fridgeDao;
        private ItemDao itemDao;
        private ItemInfoDao itemInfoDao;
        private TemperatureReadingDao temperatureReadingDao;
        private ScanEventDao scanEventDao;
        private ActivityItemDao activityItemDao;

        private PopulateDbAsyncTask(AppDatabase db) {
            fridgeDao = db.fridgeDao();
            itemDao = db.itemDao();
            itemInfoDao = db.itemInfoDao();
            temperatureReadingDao = db.temperatureReadingDao();
            scanEventDao = db.scanEventDao();
            activityItemDao = db.activityItemDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            long val = fridgeDao.insertFridge(new Fridge("Kitchen Fridge"));

            itemInfoDao.insertItemInfo(new ItemInfo("628915013498", "Great Value Extra Large Size 18 Eggs", "", 2, 5.0));
            itemInfoDao.insertItemInfo(new ItemInfo("06449348", "Excel Winterfresh Gum", "", 2, 5.0));
            itemInfoDao.insertItemInfo(new ItemInfo("792850130005", "Burts Bees Beeswax Lip Balm", "", 2, 0.0));
            itemInfoDao.insertItemInfo(new ItemInfo("664989616982", "Joe's Almonds", "", 10, 1.0));

            LocalDateTime thirdOfNovemberDate = LocalDateTime
                    .of(2019, 11, 3, 1, 1, 0);
            Date expirationDate = java.util.Date
                    .from(thirdOfNovemberDate.atZone(ZoneId.systemDefault()).toInstant());
            itemDao.insertItem(new Item("628915013498", 1, expirationDate));
            itemDao.insertItem(new Item("06449348", 1, expirationDate));
            itemDao.insertItem(new Item("792850130005", 1, expirationDate));
            itemDao.insertItem(new Item("664989616982", 1, expirationDate));

            LocalDateTime timestamp1 = LocalDateTime
                    .of(2020, 11, 3, 3, 15, 0);
            Date timestampDate1 = java.util.Date
                    .from(timestamp1.atZone(ZoneId.systemDefault()).toInstant());
            activityItemDao.insertActivity(new ActivityItem(1, "Scan In", "'Great Value Extra Large Size 18 Eggs' was added", timestampDate1));

            LocalDateTime timestamp2 = LocalDateTime
                    .of(2020, 11, 3, 4, 22, 0);
            Date timestampDate2 = java.util.Date
                    .from(timestamp2.atZone(ZoneId.systemDefault()).toInstant());
            activityItemDao.insertActivity(new ActivityItem(1, "Scan In", "'Joe's Almonds' was added", timestampDate2));

            LocalDateTime timestamp3 = LocalDateTime
                    .of(2020, 11, 3, 6, 31, 0);
            Date timestampDate3 = java.util.Date
                    .from(timestamp3.atZone(ZoneId.systemDefault()).toInstant());
            activityItemDao.insertActivity(new ActivityItem(1, "Scan In", "'PC Splendido Pecorino Romano Cheese' was added", timestampDate3));

            LocalDateTime timestamp = LocalDateTime
                    .of(2020, 6, 6, 4, 30, 0);
            Date timestampDate = java.util.Date
                    .from(timestamp.atZone(ZoneId.systemDefault()).toInstant());
            temperatureReadingDao.insertTemperatureReading(new TemperatureReading(1, timestampDate, 3.0));

            return null;
        }
    }
}