package com.unb.smartfridge.service.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.Date;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys = {
            @ForeignKey(entity = Fridge.class,
                parentColumns = "fridgeId",
                childColumns = "fridgeId",
                onDelete = CASCADE),
            @ForeignKey(entity = ItemInfo.class,
                parentColumns = "itemUpc",
                childColumns = "itemUpc",
                onDelete = CASCADE),
        })
public class Item {

    @PrimaryKey(autoGenerate = true)
    private int itemId;

    @ColumnInfo(index = true)
    private String itemUpc;

    @ColumnInfo(index = true)
    private int fridgeId;

    @ColumnInfo(index = true)
    private boolean isExpired;

    @TypeConverters(Converter.class)
    private Date expirationDate;


    public Item(String itemUpc, int fridgeId, Date expirationDate) {
        this.itemUpc = itemUpc;
        this.fridgeId = fridgeId;
        this.expirationDate = expirationDate;
        this.isExpired = false;
    }

    public void setIsExpired(boolean isExpired) {
        this.isExpired = isExpired;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getItemId() { return itemId; }

    public String getItemUpc() {
        return itemUpc;
    }

    public int getFridgeId() {
        return fridgeId;
    }

    public boolean getIsExpired() { return isExpired; }

    public Date getExpirationDate() {
        return expirationDate;
    }
}
