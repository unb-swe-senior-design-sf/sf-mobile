package com.unb.smartfridge.service.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class ItemInfo {

    @PrimaryKey
    @NonNull
    private String itemUpc;

    private String name;

    private String description;

    private int expirationInterval;

    private double storageTemperature;

    public ItemInfo(String itemUpc, String name, String description, int expirationInterval, double storageTemperature) {
        this.itemUpc = itemUpc;
        this.name = name;
        this.description = description;
        this.expirationInterval = expirationInterval;
        this.storageTemperature = storageTemperature;
    }

    public String getItemUpc() {
        return itemUpc;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getExpirationInterval() {
        return expirationInterval;
    }

    public double getStorageTemperature() {
        return storageTemperature;
    }
}
