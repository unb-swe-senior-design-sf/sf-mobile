package com.unb.smartfridge.service.background;

import java.util.concurrent.TimeUnit;

public class ExpiryUpdateSettings {

    public final static String WORK_TAG = "ExpiryWork";

    public final static int UPDATE_INTERVAL = 15;
    public final static TimeUnit UPDATE_INTERVAL_UNIT = TimeUnit.HOURS;

    public final static int FLEX_INTERVAL = 15;
    public final static TimeUnit FLEX_INTERVAL_UNIT = TimeUnit.HOURS;

}
