package com.unb.smartfridge.service.repository;

import android.app.Application;

import com.unb.smartfridge.service.model.AppDatabase;
import com.unb.smartfridge.service.model.ItemInfo;
import com.unb.smartfridge.service.model.ItemInfoDao;

import java.util.List;

public class ItemInfoRepository {

    private AppDatabase databaseService;
    private ItemInfoDao itemInfoDao;

    public ItemInfoRepository(Application application) {
        this.databaseService = AppDatabase.getInstance(application);
        itemInfoDao = databaseService.itemInfoDao();
    }

    public List<ItemInfo> getAllItemInfo() {
        return itemInfoDao.getAllItemInfo();
    }

    public ItemInfo getItemInfoById(String itemUpc) {
        return itemInfoDao.getItemInfoById(itemUpc);
    }

    public long insertItemInfo(String itemUpc, String name, String description, int expirationInterval, float storageTemperature) {
        ItemInfo newItemInfo = new ItemInfo(itemUpc, name, description, expirationInterval, storageTemperature);
        return itemInfoDao.insertItemInfo(newItemInfo);
    }

    public void deleteItemInfo(ItemInfo itemInfo) {
        itemInfoDao.deleteItemInfo(itemInfo);
    }

    public void updateItemInfo(ItemInfo itemInfo) {
        itemInfoDao.updateItemInfo(itemInfo);
    }
}
