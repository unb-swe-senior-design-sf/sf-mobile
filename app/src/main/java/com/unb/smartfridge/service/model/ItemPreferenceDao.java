package com.unb.smartfridge.service.model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ItemPreferenceDao {

    @Query("SELECT * FROM ItemPreference")
    List<ItemPreference> getAllItemPreferences();

    @Query("SELECT * FROM ItemPreference WHERE preferenceId = :preferenceId")
    ItemPreference getItemPreferenceById(int preferenceId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertItemPreference(ItemPreference itemPreference);

    @Delete
    void deleteItemPreference(ItemPreference itemPreference);

    @Update
    void updateItemPreference(ItemPreference itemPreference);
}
