package com.unb.smartfridge.view;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.unb.smartfridge.R;

public class SettingsActivity extends AppCompatActivity
        implements PreferenceFragmentCompat.OnPreferenceStartFragmentCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        // Enables the back arrow in the action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Set the root preferences as the first page to be displayed
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings_frame, new RootSettingsFragment())
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            // Return to previous screen when back arrow is pressed
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public boolean onPreferenceStartFragment(PreferenceFragmentCompat caller, Preference pref) {
        Fragment fragment;

        // Select which preference fragment to display based on preference key
        switch (pref.getKey()) {
            case "notifications_settings_fragment":
                fragment = new NotificationsSettingsFragment();
                loadFragment(fragment);
                return true;

            case "overview_settings_fragment":
                fragment = new OverviewSettingsFragment();
                loadFragment(fragment);
                return true;

            case "hardware_settings_fragment":
                fragment = new HardwareSettingsFragment();
                loadFragment(fragment);
                return true;
        }

        return false;
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.settings_frame, fragment);
        transaction.addToBackStack(null);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.commit();
    }

    public static class RootSettingsFragment extends PreferenceFragmentCompat {

        private String fragmentName = "Settings";

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);
        }

        @Override
        public void onResume() {
            super.onResume();

            //Ensure title is correct when returning to this fragment
            getActivity().setTitle(fragmentName);
        }
    }



    public static class NotificationsSettingsFragment extends PreferenceFragmentCompat {

        private String fragmentName = "Notifications";

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.notification_preferences, rootKey);
            getActivity().setTitle(fragmentName);
        }
    }

    public static class OverviewSettingsFragment extends PreferenceFragmentCompat {

        private String fragmentName = "Overview";

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.overview_preferences, rootKey);
            getActivity().setTitle(fragmentName);
        }
    }

    public static class HardwareSettingsFragment extends PreferenceFragmentCompat {

        private String fragmentName = "Hardware";

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.hardware_preferences, rootKey);
            getActivity().setTitle(fragmentName);
        }
    }
}
