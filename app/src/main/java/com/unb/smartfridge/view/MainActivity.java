package com.unb.smartfridge.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import androidx.work.WorkRequest;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.unb.smartfridge.R;
import com.unb.smartfridge.service.background.ExpiryUpdateSettings;
import com.unb.smartfridge.service.background.ExpiryWorker;
import com.unb.smartfridge.service.model.ScanEvent;
import com.unb.smartfridge.service.model.TemperatureReading;
import com.unb.smartfridge.service.model.TemperatureReadingDao;
import com.unb.smartfridge.service.repository.FridgeRepository;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fragment defaultFrag = new OverviewFragment();

        if (savedInstanceState == null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame, defaultFrag);
            transaction.addToBackStack(null);
            transaction.commit();
        }

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigationView);
        navigation.setOnNavigationItemSelectedListener(mainOnNavigationItemSelectedListener);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setOnMenuItemClickListener(mainOnToolbarMenuItemSelectedListener);

        // Background work for item expirations
        PeriodicWorkRequest expirationWork = new PeriodicWorkRequest.
                Builder(ExpiryWorker.class,
                ExpiryUpdateSettings.UPDATE_INTERVAL,
                ExpiryUpdateSettings.UPDATE_INTERVAL_UNIT)
                .build();
        WorkManager workManager = WorkManager.getInstance(getApplicationContext());
        workManager.enqueueUniquePeriodicWork(ExpiryUpdateSettings.WORK_TAG, ExistingPeriodicWorkPolicy.KEEP, expirationWork);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_overflow, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mainOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment fragment;

            switch(menuItem.getItemId()) {
                case R.id.navigation_overview:
                    fragment = new OverviewFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_activity:
                    fragment = new ActivityFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_inventory:
                    fragment = new InventoryFragment();
                    loadFragment(fragment);
                    return true;
            }
            return false;
        }
    };

    private Toolbar.OnMenuItemClickListener mainOnToolbarMenuItemSelectedListener
            = new Toolbar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(@NonNull MenuItem menuItem) {
            Fragment fragment;

            switch (menuItem.getItemId()) {
                case R.id.toolbar_settings:
                    Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                    startActivity(intent);
                    return true;
                case R.id.toolbar_temperature:
                    //TODO This code is only for testing purposes. Should be removed eventually.
                    int hour = ThreadLocalRandom.current().nextInt(1, 25);
                    int minute = ThreadLocalRandom.current().nextInt(0, 60);
                    int temperatureValue = ThreadLocalRandom.current().nextInt(-10, 11);

                    LocalDateTime timestamp = LocalDateTime
                            .of(2020, 6, 6, hour, minute, 0);
                    Date timestampDate = java.util.Date
                            .from(timestamp.atZone(ZoneId.systemDefault()).toInstant());

                    Log.d("DEBUG", "Insert new TemperatureReading.");
                    Log.d("DEBUG", "Hour: " + hour + ", Minute: " + minute + ", Value: " + temperatureValue);
                    new FridgeRepository(getApplication()).insertTemperatureReading(1, timestampDate, temperatureValue);
                    return true;
                case R.id.toolbar_scanevent:
                    //TODO This code is only for testing purposes. Should be removed eventually.
                    int hour2 = ThreadLocalRandom.current().nextInt(1, 24);
                    int minute2 = ThreadLocalRandom.current().nextInt(0, 60);
                    int month2 = ThreadLocalRandom.current().nextInt(1, 12);
                    int day2 = ThreadLocalRandom.current().nextInt(1, 31);


                    LocalDateTime timestamp2 = LocalDateTime
                            .of(2020, month2, day2, hour2, minute2, 0);
                    Date timestampDate2 = java.util.Date
                            .from(timestamp2.atZone(ZoneId.systemDefault()).toInstant());

                    Log.d("DEBUG", "Insert new Scan Event.");
                    Log.d("DEBUG", "Month: "  + month2 + " Day: " + day2 + " Hour: " + hour2 + ", Minute: " + minute2);
                    new FridgeRepository(getApplication()).insertScanEvent(1, timestampDate2, true);

                    return true;
            }

            return false;
        }
    };

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment);
        transaction.addToBackStack(null);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.commit();
    }
}
