package com.unb.smartfridge.view.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.unb.smartfridge.R;
import com.unb.smartfridge.service.model.ActivityItem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ActivityAdapter extends RecyclerView.Adapter<ActivityAdapter.ActivityHolder> {

    private final String TAG = "ActivityAdapter";
    private List<ActivityItem> activities = new ArrayList<>();

    @NonNull
    @Override
    public ActivityHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_item, parent, false);
        return new ActivityHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ActivityHolder holder, int position) {
        ActivityItem currentActivity = activities.get(position);
        holder.textViewTitle.setText(currentActivity.getTitle());
        holder.textViewDescription.setText(currentActivity.getDescription());

        Date timestamp = currentActivity.getTimestamp();
        DateFormat dateFormatter = new SimpleDateFormat("h:mm a, MMM d");
        holder.textViewTimestamp.setText(dateFormatter.format(timestamp));
    }

    @Override
    public int getItemCount() {
        return activities.size();
    }

    public void setActivities(List<ActivityItem> activities) {
        Log.d(TAG, "Setting the list of ActivityItems");
        this.activities = activities;
        notifyDataSetChanged();
    }

    class ActivityHolder extends RecyclerView.ViewHolder {
        private TextView textViewTitle;
        private TextView textViewDescription;
        private TextView textViewTimestamp;

        public ActivityHolder(View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.activity_title);
            textViewDescription = itemView.findViewById(R.id.activity_description);
            textViewTimestamp = itemView.findViewById(R.id.activity_timestamp);
        }
    }
}
