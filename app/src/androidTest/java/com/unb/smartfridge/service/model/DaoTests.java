package com.unb.smartfridge.service.model;

import android.content.Context;

import androidx.room.Room;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class DaoTests {

    private FridgeDao fridgeDao;
    private TemperatureReadingDao temperatureReadingDao;
    private ItemInfoDao itemInfoDao;
    private AppDatabase db;

    @Before
    public void createDb() {
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        db = Room.inMemoryDatabaseBuilder(context,
                AppDatabase.class)
                .allowMainThreadQueries()
                .build();
        fridgeDao = db.fridgeDao();
        fridgeDao.insertFridge(new Fridge("Main Fridge"));
        temperatureReadingDao = db.temperatureReadingDao();
        itemInfoDao = db.itemInfoDao();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    @Test
    public void testSelectScanEventsBetweenDates() throws Exception {
        int numTimestamps = 10;
        int dayOfMonth = 1;

        for (int i = 0; i < numTimestamps; i++) {
            LocalDateTime timestamp = LocalDateTime.of(2020, 11, dayOfMonth, 1, 1, 0);
            dayOfMonth++;

            TemperatureReading reading = new TemperatureReading(0, 1,
                    java.util.Date.from(timestamp.atZone(ZoneId.systemDefault()).toInstant()),
                    5.0);
            temperatureReadingDao.insertTemperatureReading(reading);
        }

        LocalDateTime thirdOfNovemberDate = LocalDateTime
                .of(2020, 11, 3, 1, 1, 0);
        Date thirdOfNovember = java.util.Date
                .from(thirdOfNovemberDate.atZone(ZoneId.systemDefault()).toInstant());

        LocalDateTime eighthOfNovemberDate = LocalDateTime
                .of(2020, 11, 8, 1, 1, 0);
        Date eighthOfNovember = java.util.Date
                .from(eighthOfNovemberDate.atZone(ZoneId.systemDefault()).toInstant());

        List<TemperatureReading> dates =
                temperatureReadingDao.getTemperatureReadingsBetweenDates(thirdOfNovember, eighthOfNovember);

        // Expect November 3, 4, 5, 6, 7 and 8 to be returned (6)
        assertEquals(6, dates.size());
    }

    @Test
    public void testGetItemInfoById() throws Exception {
        String targetUpc = "792850130005";
        String univeralProductCodes[] = {"06493430008", "792850130005", "628915013498"};

        for (int i = 0; i < univeralProductCodes.length; i++) {
            String name = "TestItem" + i;
            ItemInfo itemInfo = new ItemInfo(univeralProductCodes[i], name, "Desc", 1, 1);
            itemInfoDao.insertItemInfo(itemInfo);
        }

        ItemInfo foundItemInfo = itemInfoDao.getItemInfoById(targetUpc);
        assertEquals(targetUpc, foundItemInfo.getItemUpc());
    }
}
