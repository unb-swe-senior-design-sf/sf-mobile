package com.unb.smartfridge;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect_1() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void addition_isCorrect_2() {
        assertEquals(7, 4 + 3);
    }
}